package com.fmi.coffee;

import com.fmi.coffee.dto.CoffeeDto;
import com.fmi.coffee.dto.CoffeeOrderDto;
import com.fmi.coffee.dto.OrderRequestDto;
import com.fmi.coffee.entity.enums.CoffeeSpecies;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import com.fmi.coffee.service.OrderService;
import com.fmi.coffee.service.CoffeeService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;

import java.text.DecimalFormat;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
@ActiveProfiles({ "development" })
public class CoffeeOrderServiceTest {

    @Resource
    private CoffeeService coffeeService;
    private Long coffeeId;
    DecimalFormat df = new DecimalFormat("0.00");

    @Resource
    private OrderService orderService;

    @Rule
    public ExpectedException ex =  ExpectedException.none();

    @Before
    public void init() {
        CoffeeDto coffee = coffeeService.create(this.getCoffee());
        coffeeId = coffee.getId();
    }

    @Test
    public void testCreateEspresso() {
        CoffeeOrderDto res = orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Espresso).coffeeId(coffeeId).build());
        assertNotNull(res.getId());
        assertEquals(CoffeeType.Espresso, res.getType());
        assertEquals("French Mission", res.getCoffee().getName());
        assertEquals(30, res.getAmount());
        assertEquals("1.73", df.format(res.getDeliveryPrice()));
        assertEquals("2.20", df.format(res.getMarketPrice()));
    }

    @Test
    public void testCreateAmericano() {
        CoffeeOrderDto res = orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Americano).coffeeId(coffeeId).build());
        assertNotNull(res.getId());
        assertEquals(CoffeeType.Americano, res.getType());
        assertEquals("French Mission", res.getCoffee().getName());
        assertEquals("Africa", res.getCoffee().getRegion());
        assertEquals(CoffeeSpecies.Arabica, res.getCoffee().getSpecies());
        assertEquals("1.73", df.format(res.getDeliveryPrice()));
        assertEquals("2.40", df.format(res.getMarketPrice()));
        assertEquals(120, res.getAmount());
    }

    @Test
    public void testCreateCappucino() {
        CoffeeOrderDto res = orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Cappucino).coffeeId(coffeeId).milkType(MilkType.Almond).build());
        assertNotNull(res.getId());
        assertEquals(CoffeeType.Cappucino, res.getType());
        assertEquals("Africa", res.getCoffee().getRegion());
        assertEquals(150, res.getAmount());
        assertEquals(MilkType.Almond, res.getMilk());
        assertEquals("3.80", df.format(res.getMarketPrice()));
        assertEquals("2.73", df.format(res.getDeliveryPrice()));
    }

    @Test
    public void testCreateLatte() {
        CoffeeOrderDto res = orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Latte).coffeeId(coffeeId).milkType(MilkType.Milk).build());
        assertNotNull(res.getId());
        assertEquals(CoffeeType.Latte, res.getType());
        assertEquals(CoffeeSpecies.Arabica, res.getCoffee().getSpecies());
        assertEquals(210, res.getAmount());
        assertEquals(MilkType.Milk, res.getMilk());
        assertEquals("4.50", df.format(res.getMarketPrice()));
        assertEquals("3.23", df.format(res.getDeliveryPrice()));
    }

    @Test
    public void testGetAll() {
        orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Espresso).coffeeId(coffeeId).build());
        orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Americano).coffeeId(coffeeId).build());
        orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Cappucino).coffeeId(coffeeId).milkType(MilkType.Almond).build());
        orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Latte).coffeeId(coffeeId).milkType(MilkType.Milk).build());

        Pageable pageable = PageRequest.of(0, 2);
        Page<CoffeeOrderDto> page = orderService.get(pageable);
        assertEquals(2, page.getTotalPages());
        assertEquals(2, page.getContent().size());
        assertEquals(4, page.getTotalElements());
        assertEquals(CoffeeType.Espresso, page.getContent().get(0).getType());
        assertEquals(CoffeeType.Americano, page.getContent().get(1).getType());

        pageable = PageRequest.of(1, 2);
        page = orderService.get(pageable);
        assertEquals(CoffeeType.Cappucino, page.getContent().get(0).getType());
        assertEquals(CoffeeType.Latte, page.getContent().get(1).getType());
    }

    @Test
    public void testSoftDelete() {
        Long id = orderService.create(OrderRequestDto.builder().coffeeType(CoffeeType.Cappucino).coffeeId(coffeeId).milkType(MilkType.Almond).build()).getId();

        CoffeeOrderDto res = orderService.get(id);
        assertEquals(CoffeeType.Cappucino, res.getType());

        orderService.softDelete(id);

        ex.expect(EntityNotFoundException.class);
        ex.expectMessage("Cannot find coffee drink with id " + id);
        orderService.get(id);
    }

    private CoffeeDto getCoffee() {
        return CoffeeDto.builder()
                .name("French Mission")
                .region("Africa")
                .species(CoffeeSpecies.Arabica)
                .deliveryPrice(1.73)
                .build();
    }

}
