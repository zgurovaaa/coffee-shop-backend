package com.fmi.coffee;

import com.fmi.coffee.dto.CoffeeDto;
import com.fmi.coffee.entity.enums.CoffeeSpecies;
import com.fmi.coffee.service.CoffeeService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

@SpringBootTest
@RunWith(SpringRunner.class)
@Transactional
@ActiveProfiles({ "development" })
public class CoffeeServiceTest {

    @Resource
    private CoffeeService coffeeService;
    @Rule
    public ExpectedException expectedEx = ExpectedException.none();
    DecimalFormat df = new DecimalFormat("0.00");


    @Test
    public void testCreateAndGetCoffee() {
        Long id = coffeeService.create(getValidEthiopianHararCoffee()).getId();
        CoffeeDto res = coffeeService.get(id);
        assertEquals(CoffeeSpecies.Arabica, res.getSpecies());
        assertEquals("Ethiopia", res.getRegion());
        assertEquals("Ethiopian Harar", res.getName());
        assertEquals("2.30", df.format(res.getDeliveryPrice()));
    }

    @Test
    public void testBatchInsertAndGetAll() {
        coffeeService.batchInsert(Arrays.asList(getValidEthiopianHararCoffee(), getValidGeishaCoffee(), getValidPacasCoffee()));

        Pageable pageable = PageRequest.of(0, 5);
        Page<CoffeeDto> page = coffeeService.get(pageable);
        assertEquals(3, page.getTotalElements());
        assertEquals(1, page.getTotalPages());
        assertEquals("Costa Rica", page.getContent().get(1).getRegion());
    }

    @Test
    public void testEdit() {
        CoffeeDto coffeeDto = getValidPacasCoffee();
        CoffeeDto res = coffeeService.create(coffeeDto);
        assertEquals("Pacas", res.getName());
        assertEquals("Latin America", res.getRegion());
        Long id = res.getId();

        coffeeDto.setName("new name");
        coffeeService.edit(id, coffeeDto);
        res = coffeeService.get(id);
        assertEquals("new name", res.getName());
    }

    @Test
    public void testSoftDelete() {
        List<CoffeeDto> res = coffeeService.batchInsert(Arrays.asList(getValidEthiopianHararCoffee(), getValidGeishaCoffee(), getValidPacasCoffee()));
        Long id = res.get(1).getId();

        coffeeService.softDelete(id);

        Pageable pageable = PageRequest.of(0, 5);
        Page<CoffeeDto> page = coffeeService.get(pageable);
        assertEquals(2, page.getTotalElements());

        expectedEx.expect(EntityNotFoundException.class);
        expectedEx.expectMessage("Cannot find coffee with id " + id);
        coffeeService.get(id);

    }

    private CoffeeDto getValidEthiopianHararCoffee() {
        return CoffeeDto.builder()
                .species(CoffeeSpecies.Arabica)
                .region("Ethiopia")
                .name("Ethiopian Harar")
                .deliveryPrice(2.3).build();
    }

    private CoffeeDto getValidGeishaCoffee() {
        return CoffeeDto.builder()
                .species(CoffeeSpecies.Arabica)
                .region("Costa Rica")
                .name("Geisha")
                .deliveryPrice(2.5).build();
    }

    private CoffeeDto getValidPacasCoffee() {
        return CoffeeDto.builder()
                .species(CoffeeSpecies.Arabica)
                .region("Latin America")
                .name("Pacas")
                .deliveryPrice(1.7).build();
    }
}
