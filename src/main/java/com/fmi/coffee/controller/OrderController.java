package com.fmi.coffee.controller;

import com.fmi.coffee.dto.CoffeeOrderDto;
import com.fmi.coffee.dto.OrderListDto;
import com.fmi.coffee.dto.OrderRequestDto;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import com.fmi.coffee.service.OrderService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.stream.Collectors;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping(value = "/coffee-order", produces = "application/json")
public class OrderController {

    @Resource
    private OrderService service;

    @PostMapping
    public CoffeeOrderDto create(@RequestParam CoffeeType type,@RequestParam Long coffeeId,@RequestParam(required = false) MilkType milkType ) {
        OrderRequestDto orderRequest = OrderRequestDto.builder().coffeeType(type).coffeeId(coffeeId).milkType(milkType).build();
        return service.create(orderRequest);
    }

    @GetMapping(path = "/{id}")
    public CoffeeOrderDto get(@PathVariable Long id) {
        return service.get(id);
    }

    @GetMapping
    public OrderListDto get(Pageable pageable) {
        Page<CoffeeOrderDto> res = service.get(pageable);
        double delPriceTotal = res.getContent().stream().map(CoffeeOrderDto::getDeliveryPrice).collect(Collectors.toList()).stream().mapToDouble(Double::doubleValue).sum();
        double marketPriceTotal = res.getContent().stream().map(CoffeeOrderDto::getMarketPrice).collect(Collectors.toList()).stream().mapToDouble(Double::doubleValue).sum();
        return OrderListDto.builder().totalCount(res.getTotalElements()).orders(res.getContent()).total(delPriceTotal).profit(marketPriceTotal - delPriceTotal).build();
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable Long id) {
        service.softDelete(id);
    }
}
