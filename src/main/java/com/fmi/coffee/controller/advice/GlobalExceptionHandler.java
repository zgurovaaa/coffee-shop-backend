package com.fmi.coffee.controller.advice;

import com.fmi.coffee.dto.MessageDto;
import com.fmi.coffee.dto.ResponseCode;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.persistence.EntityNotFoundException;

@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<MessageDto> entityNotFoundException(EntityNotFoundException e) {
        return new ResponseEntity<>(
                MessageDto.builder()
                        .code(ResponseCode.NotFound)
                        .msg(e.getMessage())
                        .details(ExceptionUtils.getStackTrace(e)).build(),
                HttpStatus.NOT_FOUND
        );
    }
}
