package com.fmi.coffee.controller;

import com.fmi.coffee.dto.CoffeeDto;
import com.fmi.coffee.dto.CoffeeListDto;
import com.fmi.coffee.service.CoffeeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping(value = "/coffee", produces = "application/json")
public class CoffeeController {

    @Resource
    private CoffeeService coffeeService;

    @PostMapping
    public CoffeeDto create(@Valid @RequestBody CoffeeDto source) {
        return coffeeService.create(source);
    }

    @PostMapping(path = "/batch-insert")
    public CoffeeListDto batchInsert(@RequestBody List<CoffeeDto> source) {
        List<CoffeeDto> res = coffeeService.batchInsert(source);
        return CoffeeListDto.builder().totalCount(res.size()).coffees(res).build();
    }

    @GetMapping(path = "/{id}")
    public CoffeeDto get(@PathVariable Long id) {
        return coffeeService.get(id);
    }

    @GetMapping
    public CoffeeListDto get(Pageable pageable) {
        Page<CoffeeDto> page = coffeeService.get(pageable);
        return CoffeeListDto.builder().totalCount(page.getTotalElements()).coffees(page.getContent()).build();
    }

    @PutMapping(path = "/{id}")
    public CoffeeDto edit(@PathVariable Long id, @RequestBody CoffeeDto coffee) {
        return coffeeService.edit(id, coffee);
    }

    @DeleteMapping(path = "/{id}")
    public void delete(@PathVariable Long id) {
        coffeeService.softDelete(id);
    }
}
