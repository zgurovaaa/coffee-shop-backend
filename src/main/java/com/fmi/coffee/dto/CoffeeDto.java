package com.fmi.coffee.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fmi.coffee.entity.enums.CoffeeSpecies;
import lombok.*;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CoffeeDto {

    private Long id;
    @NotNull
    private CoffeeSpecies species;
    @NotBlank
    private String name;
    @NotBlank
    private String region;
    @NotNull
    private double deliveryPrice;
    @JsonIgnore
    private Set<CoffeeOrderDto> orders;
}
