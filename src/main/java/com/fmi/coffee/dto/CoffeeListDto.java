package com.fmi.coffee.dto;

import lombok.*;

import java.util.List;

@Data
@Builder
public class CoffeeListDto {

    private long totalCount;
    private List<CoffeeDto> coffees;
}
