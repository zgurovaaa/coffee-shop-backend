package com.fmi.coffee.dto;

import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import lombok.*;

import javax.validation.constraints.NotNull;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CoffeeOrderDto {

    private Long id;
    @NotNull
    private CoffeeType type;
    private CoffeeDto coffee;
    private MilkType milk;
    private int amount;
    private String otherIng;
    @NotNull
    private double deliveryPrice;
    @NotNull
    private double marketPrice;
}
