package com.fmi.coffee.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class OrderListDto {

    private long totalCount;
    private List<CoffeeOrderDto> orders;
    private double total;
    private double profit;
}
