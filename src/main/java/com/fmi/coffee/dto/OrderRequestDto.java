package com.fmi.coffee.dto;

import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class OrderRequestDto {
    private CoffeeType coffeeType;
    private Long coffeeId;
    private MilkType milkType;
}
