package com.fmi.coffee.repository;

import com.fmi.coffee.entity.CoffeeOrder;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<CoffeeOrder, Long> {

    Optional<CoffeeOrder> findByIdAndDeletedFalse(Long id);

    Page<CoffeeOrder> findAllByDeletedFalse(Pageable pageable);

}
