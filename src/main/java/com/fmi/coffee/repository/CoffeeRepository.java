package com.fmi.coffee.repository;

import com.fmi.coffee.entity.Coffee;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;


@Repository
public interface CoffeeRepository extends JpaRepository<Coffee, Long> {

    Optional<Coffee> findByIdAndDeletedFalse(Long id);

    Page<Coffee> findAllByDeletedFalse(Pageable pageable);
}
