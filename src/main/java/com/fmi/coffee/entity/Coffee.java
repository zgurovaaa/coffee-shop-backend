package com.fmi.coffee.entity;

import com.fmi.coffee.entity.enums.CoffeeSpecies;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class Coffee {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private CoffeeSpecies species;

    @Column(unique = true)
    private String name;
    private String region;
    private double deliveryPrice;

    @OneToMany(mappedBy = "coffee")
    @Builder.Default
    private Set<CoffeeOrder> coffeeOrders = new HashSet<>();
    private boolean deleted;

}
