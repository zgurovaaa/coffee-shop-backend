package com.fmi.coffee.entity.enums;

public enum CoffeeSpecies {
    Arabica, Robusta
}
