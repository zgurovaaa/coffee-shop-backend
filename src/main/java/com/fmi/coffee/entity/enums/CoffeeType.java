package com.fmi.coffee.entity.enums;

public enum CoffeeType {
    Espresso,
    Macciato,
    Americano,
    Lungo,
    Mocha,
    Cappucino,
    Latte,
    Romano,
    IrishCoffee

}
