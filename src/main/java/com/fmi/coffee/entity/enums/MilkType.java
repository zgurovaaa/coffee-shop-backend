package com.fmi.coffee.entity.enums;

public enum MilkType {
    Milk,
    Almond,
    Coconut,
    LactoseFree

}
