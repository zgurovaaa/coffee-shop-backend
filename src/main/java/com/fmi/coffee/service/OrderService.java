package com.fmi.coffee.service;

import com.fmi.coffee.dto.CoffeeOrderDto;
import com.fmi.coffee.dto.OrderRequestDto;
import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.repository.OrderRepository;
import com.fmi.coffee.service.builder.OrderBuilderService;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class OrderService {

    @Resource
    private OrderRepository repository;

    @Resource
    private CoffeeService coffeeService;

    @Resource
    private List<OrderBuilderService> orderBuilderServices;
    private Map<CoffeeType, OrderBuilderService> builderMap = new HashMap<>();

    @PostConstruct
    public void init() {
        orderBuilderServices.forEach(
                s -> builderMap.put(s.getType(), s));
    }

    public CoffeeOrderDto create(OrderRequestDto coffeeDrinkRequest) {
        Coffee coffee = coffeeService.getEntity(coffeeDrinkRequest.getCoffeeId());
        CoffeeOrder coffeeOrder = repository.save(builderMap.get(coffeeDrinkRequest.getCoffeeType()).buildDrink(coffee, coffeeDrinkRequest.getMilkType()));
        return entityToDto(coffeeOrder, true);
    }

    public CoffeeOrderDto get(Long id) {
        return entityToDto(getEntity(id), true);
    }

    public Page<CoffeeOrderDto> get(Pageable pageable) {
        Page<CoffeeOrder> res = repository.findAllByDeletedFalse(pageable);
        return new PageImpl<>(entityToDto(res.getContent(), true), pageable, res.getTotalElements());
    }


    public void softDelete(Long id) {
        CoffeeOrder res = getEntity(id);
        res.setDeleted(true);
        repository.save(res);
    }

    private CoffeeOrder getEntity(Long id) {
        return repository.findByIdAndDeletedFalse(id).orElseThrow(() -> new EntityNotFoundException("Cannot find coffee drink with id " + id));
    }

    private static CoffeeOrder dtoToEntity(CoffeeOrderDto source, boolean withCoffee) {
        CoffeeOrder res = new CoffeeOrder();
        BeanUtils.copyProperties(source, res, "coffee");
        if (withCoffee) {
            res.setCoffee(CoffeeService.dtoToEntity(source.getCoffee(), false));
        }
        return res;
    }

    static Set<CoffeeOrder> dtoToEntity(Set<CoffeeOrderDto> source, boolean withCoffee) {
        return source.stream().map(c -> dtoToEntity(c, withCoffee)).collect(Collectors.toSet());
    }

    private static CoffeeOrderDto entityToDto(CoffeeOrder source, boolean withCoffee) {
        CoffeeOrderDto res = new CoffeeOrderDto();
        BeanUtils.copyProperties(source, res, "coffee");
        if (withCoffee) {
            res.setCoffee(CoffeeService.entityToDto(source.getCoffee(), false));
        }
        return res;
    }

    static Set<CoffeeOrderDto> entityToDto(Set<CoffeeOrder> source, boolean withCoffee) {
        return source.stream().map(c -> entityToDto(c, withCoffee)).collect(Collectors.toSet());
    }

    private static List<CoffeeOrderDto> entityToDto(List<CoffeeOrder> source, boolean withCoffee) {
        return source.stream().map(c -> entityToDto(c, withCoffee)).collect(Collectors.toList());
    }

}
