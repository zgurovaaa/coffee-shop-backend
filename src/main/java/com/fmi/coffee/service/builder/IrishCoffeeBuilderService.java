package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class IrishCoffeeBuilderService extends OrderBuilderService {
    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = coffee.getDeliveryPrice() + 3;
        CoffeeOrder irishCoffee = CoffeeOrder.builder()
                .type(CoffeeType.IrishCoffee)
                .coffee(coffee)
                .milk(milkType)
                .amount(200)
                .otherIng("50ml irish whiskey, 1tsp brown sugar")
                .build();
        return irishCoffee;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.IrishCoffee;
    }
}
