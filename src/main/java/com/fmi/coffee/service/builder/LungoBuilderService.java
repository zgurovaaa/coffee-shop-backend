package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class LungoBuilderService extends OrderBuilderService {
    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = Math.round( (0.3 * coffee.getDeliveryPrice() + coffee.getDeliveryPrice()) * 10.0) / 10.0;
        CoffeeOrder lungo = CoffeeOrder.builder()
                .type(CoffeeType.Lungo)
                .coffee(coffee)
                .amount(70)
                .deliveryPrice(coffee.getDeliveryPrice())
                .marketPrice(price)
                .build();
        return lungo;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Lungo;
    }
}
