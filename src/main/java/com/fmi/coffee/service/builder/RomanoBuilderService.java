package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class RomanoBuilderService extends OrderBuilderService {

    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = coffee.getDeliveryPrice() + 0.5;
        CoffeeOrder romano = CoffeeOrder.builder()
                .type(CoffeeType.Romano)
                .coffee(coffee)
                .amount(40)
                .deliveryPrice(price)
                .marketPrice(Math.round( (0.4 * coffee.getDeliveryPrice() + coffee.getDeliveryPrice()) * 10.0) / 10.0)
                .otherIng("1 strip of a lemon peel")
                .build();
        return romano;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Romano;
    }

}
