package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class MacciatoBuilderService extends OrderBuilderService {
    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = coffee.getDeliveryPrice() + 1;
        CoffeeOrder macciato = CoffeeOrder.builder()
                .type(CoffeeType.Macciato)
                .coffee(coffee)
                .amount(70)
                .milk(milkType)
                .deliveryPrice(price)
                .marketPrice(Math.round((0.4 * price + price) * 10.0) / 10.0)
                .build();
        return macciato;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Macciato;
    }
}
