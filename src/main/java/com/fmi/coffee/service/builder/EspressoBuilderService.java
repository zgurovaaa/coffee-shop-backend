package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class EspressoBuilderService extends OrderBuilderService {

    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = Math.round( (0.3 * coffee.getDeliveryPrice() + coffee.getDeliveryPrice()) * 10.0) / 10.0;
        CoffeeOrder espresso = CoffeeOrder.builder()
                .type(CoffeeType.Espresso)
                .coffee(coffee)
                .amount(30)
                .deliveryPrice(coffee.getDeliveryPrice())
                .marketPrice(price).build();

        return espresso;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Espresso;
    }

}
