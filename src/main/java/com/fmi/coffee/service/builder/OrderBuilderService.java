package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;

public abstract class OrderBuilderService {

    public abstract CoffeeOrder buildDrink(Coffee coffee, MilkType milkType);

    public abstract CoffeeType getType();

}
