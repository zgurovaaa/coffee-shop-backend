package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class CappucinoBuilderService extends OrderBuilderService {

    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = coffee.getDeliveryPrice() + 1;
        CoffeeOrder cappucino = CoffeeOrder.builder()
                .type(CoffeeType.Cappucino)
                .coffee(coffee)
                .amount(150)
                .milk(milkType)
                .deliveryPrice(price)
                .marketPrice(Math.round( (0.4 * price + price) * 10.0) / 10.0)
                .build();
        return cappucino;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Cappucino;
    }

}
