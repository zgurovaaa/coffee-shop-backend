package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class LatteBuilderService extends OrderBuilderService {

    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        double price = coffee.getDeliveryPrice() + 1.5;
        CoffeeOrder latte = CoffeeOrder.builder()
                .type(CoffeeType.Latte)
                .coffee(coffee)
                .amount(210)
                .milk(milkType)
                .deliveryPrice(price)
                .marketPrice(Math.round( (0.4 * price + price) * 10.0) / 10.0)
                .build();
        return latte;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Latte;
    }

}
