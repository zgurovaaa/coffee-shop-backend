package com.fmi.coffee.service.builder;

import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.entity.CoffeeOrder;
import com.fmi.coffee.entity.enums.CoffeeType;
import com.fmi.coffee.entity.enums.MilkType;
import org.springframework.stereotype.Service;

@Service
public class AmericanoBuilderService extends OrderBuilderService {

    @Override
    public CoffeeOrder buildDrink(Coffee coffee, MilkType milkType) {
        CoffeeOrder americano = CoffeeOrder.builder()
                .type(CoffeeType.Americano)
                .coffee(coffee)
                .amount(120)
                .deliveryPrice(coffee.getDeliveryPrice())
                .marketPrice(Math.round( (0.4 * coffee.getDeliveryPrice() + coffee.getDeliveryPrice()) * 10.0) / 10.0)
                .build();

        return americano;
    }

    @Override
    public CoffeeType getType() {
        return CoffeeType.Americano;
    }

}
