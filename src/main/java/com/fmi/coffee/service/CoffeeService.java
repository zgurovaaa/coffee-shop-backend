package com.fmi.coffee.service;

import com.fmi.coffee.dto.CoffeeDto;
import com.fmi.coffee.entity.Coffee;
import com.fmi.coffee.repository.CoffeeRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
@CacheConfig(cacheNames = { "coffees" })
public class CoffeeService {

    @Resource
    private CoffeeRepository repository;

    @Caching(put = {
            @CachePut(value = "coffees")
    })
    public CoffeeDto create(CoffeeDto source) {
        Coffee res = repository.save(dtoToEntity(source, false));
        return entityToDto(res, false);
    }

    @CachePut(value = "coffees")
    public List<CoffeeDto> batchInsert(List<CoffeeDto> source) {
        List<Coffee> res = repository.saveAll(dtoToEntity(source, false));
        return entityToDto(res, false);
    }

    @Cacheable("coffees")
    public CoffeeDto get(Long id) {
        return entityToDto(getEntity(id), false);
    }

    @Cacheable("coffees")
    public Page<CoffeeDto> get(Pageable pageable) {
        Page<Coffee> res = repository.findAllByDeletedFalse(pageable);
        return new PageImpl<>(entityToDto(res.getContent(), false), pageable, res.getTotalElements());
    }

    public CoffeeDto edit(Long id, CoffeeDto coffee) {
        Coffee res = this.getEntity(id);
        Coffee source = dtoToEntity(coffee, false);
        BeanUtils.copyProperties(source, res, "id");
        res = repository.save(res);
        return entityToDto(res, false);
    }

    @Caching(evict = {
            @CacheEvict(value = "coffees", allEntries = true)
    })
    public void softDelete(Long id) {
        Coffee res = getEntity(id);
        res.setDeleted(true);
        repository.save(res);
    }

    Coffee getEntity(Long id) {
        return repository.findByIdAndDeletedFalse(id).orElseThrow(() -> new EntityNotFoundException("Cannot find coffee with id " + id));
    }

    static Coffee dtoToEntity(CoffeeDto source, boolean withCoffeeDrinks) {
        Coffee res = new Coffee();
        BeanUtils.copyProperties(source, res, "coffeeOrders");
        if (withCoffeeDrinks) {
            res.setCoffeeOrders(OrderService.dtoToEntity(source.getOrders(), false));
        }
        return res;
    }

    private static List<Coffee> dtoToEntity(List<CoffeeDto> source, boolean withCoffeeDrinks) {
        return source.stream().map(c -> dtoToEntity(c, withCoffeeDrinks)).collect(Collectors.toList());
    }

    static CoffeeDto entityToDto(Coffee source, boolean withCoffeeDrinks) {
        CoffeeDto res = new CoffeeDto();
        BeanUtils.copyProperties(source, res, "coffeeOrders");
        if (withCoffeeDrinks) {
            res.setOrders(OrderService.entityToDto(source.getCoffeeOrders(), false));
        }
        return res;
    }

    static Set<CoffeeDto> entityToDto(Set<Coffee> source, boolean withCoffeeDrinks) {
        return source.stream().map(c -> entityToDto(c, withCoffeeDrinks)).collect(Collectors.toSet());
    }

    private static List<CoffeeDto> entityToDto(List<Coffee> source, boolean withCoffeeDrinks) {
        return source.stream().map(c -> entityToDto(c, withCoffeeDrinks)).collect(Collectors.toList());
    }

}
